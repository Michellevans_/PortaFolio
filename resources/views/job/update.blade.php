
@extends ('admin.template.main')
	@section('title','Home')  
	
		@section('content')

         	{!! Form::open(['route' => 'jobs/update', 'method' => 'PUT']) !!}

         	   <h1>Trabajos</h1>

              <div class="form-group">
              	
              	<label class="control-label">A contianuación  podra actualizar los trabajos:</label>
              </div>

              <div class="form-group">
                {!! Form::hidden('id', $job->id) !!}
              
                  {!! Form::label('jot_title','Titulo del Trabajo') !!}
                  {!! Form::text('job_title',$job->job_title,['class'=> 'form-control','placeholder'=>'','required']) !!}
              
              </div>

              
               <div class="form-group">
              
                  {!! Form::label('deadline','Fecha de Finalizacion') !!}
                  {!! Form::text('deadline',$job->deadline,['class'=> 'form-control txtArea','placeholder'=>'','required']) !!}
              
              </div>

              <div class="form-group">
              
                  {!! Form::label('job_description','Descripcion del Trabajo') !!}
                  {!! Form::text('job_description',$job->job_description,['class'=> 'form-control','placeholder'=>'','required']) !!}
              
              </div>
               <div class="form-group">
              
              		{!! Form::submit('Actualizar',['class'=>'btn btn-primary']) !!}
              		
              
              </div>

         	{!! Form::close() !!}

		@endsection

	
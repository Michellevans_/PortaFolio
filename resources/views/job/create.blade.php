
@extends ('admin.template.main')
	@section('title','Home')  
	
		@section('content')

         	{!! Form::open(['route' => 'jobs.store', 'method' => 'POST']) !!}

         	   <h1>Trabajos</h1>

              <div class="form-group">
              	
              	<label class="control-label">A contianuación  podra registrar los trabajos:</label>
              </div>

              <div class="form-group">
              
              		{!! Form::label('jot_title','Titulo del Trabajo') !!}
              		{!! Form::text('job_title',null,['class'=> 'form-control','placeholder'=>'','required']) !!}
              
              </div>

              
               <div class="form-group">
              
              		{!! Form::label('deadline','Fecha de Finalizacion') !!}
              		{!! Form::text('deadline',null,['class'=> 'form-control txtArea','placeholder'=>'','required']) !!}
              
              </div>

              <div class="form-group">
              
                  {!! Form::label('job_description','Descripcion del Trabajo') !!}
                  {!! Form::text('job_description',null,['class'=> 'form-control','placeholder'=>'','required']) !!}
              
              </div>
          
               <div class="form-group">
              
              		{!! Form::submit('Registrar',['class'=>'btn btn-primary']) !!}
              		
              
              </div>

         	{!! Form::close() !!}

		@endsection
	
@extends('admin.template.main')
@section('content')
	<div>
		<a class="btn btn-success" href="{{ route('jobs.create') }}">Crear</a>
		<hr>
		<section>
			<table class="table table-hover">
				<thead class="thead-inverse">
					<tr>
						<th>Titulo</th>						
						<th>Fecha de Finalización</th>
						<th>Descripción</th>  
					</tr>
				</thead>
				<tbody>
					@foreach($job as $value)
						<tr>
							<td>{{ $value->job_title }}</td>	
							<td>{{ $value->deadline }}</td>				
							<td>{{ $value->job_description }}</td>
							<td>
								<a class="btn btn-primary btn-xs" href="{{ route('jobs.edit',['id' => $value->id] )}}" >
									Editar
								</a> 
							</td> 
						</tr>
					@endforeach
				</tbody>
			</table>
	</div>
@endsection
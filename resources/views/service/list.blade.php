@extends('admin.template.main')
@section('content')
	<div>
		<a class="btn btn-success" href="{{ route('services.create') }}">Crear</a>
		<hr>
		<section>
			<table class="table table-hover">
				<thead class="thead-inverse">
					<tr>
						<th>Nombre del Servicio</th>						
						<th>Descripción</th>
						<th>Fecha</th>   
					</tr>
				</thead>
				<tbody>
					@foreach($service as $value)
						<tr>
							<td>{{ $value->name_servicies }}</td>	
							<td>{{ $value->date }}</td>				
							<td>
								<a class="btn btn-primary btn-xs" href="{{ route('services.edit',['id' => $value->id] )}}" >
									Editar
								</a> 
							</td> 
						</tr>
					@endforeach
				</tbody>
			</table>
	</div>
@endsection
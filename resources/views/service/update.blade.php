
@extends ('admin.template.main')
	@section('title','Home')  
	
		@section('content')

         	{!! Form::open(['route' => 'services/update', 'method' => 'PUT']) !!}

         	   <h1>Servicios</h1>

              <div class="form-group">
              	
              	<label class="control-label">A contianuación  podra registrar servicios:</label>
              </div>

              <div class="form-group">
                {!! Form::hidden('id', $service->id) !!}
              
              		{!! Form::label('name_services','Nombre del Servicio') !!}
              		{!! Form::text('name_services',$service->name_servicies,['class'=> 'form-control','placeholder'=>'','required']) !!}
              
              </div>

              
               <div class="form-group">
              
              		{!! Form::label('description','Descripcion') !!}
              		{!! Form::textarea('description',$service->description,['class'=> 'form-control txtArea','placeholder'=>'','required']) !!}
              
              </div>

              <div class="form-group">
              
                  {!! Form::label('date','Fecha del Servicio') !!}
                  {!! Form::text('date',$service->date,['class'=> 'form-control','placeholder'=>'Ejemplo: 2018','required']) !!}
              
              </div>
          
               <div class="form-group">
              
              		{!! Form::submit('Actualizar',['class'=>'btn btn-primary']) !!}
              		
              
              </div>

         	{!! Form::close() !!}

		@endsection
    @section('js')
      <script>
        $('.txtArea').trumbowyg();
      </script>
    @endsection
	
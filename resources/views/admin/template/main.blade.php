<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title> @yield('tittle','Default') |Panel de Administración</title>
	<link rel="stylesheet"  href="{{asset('plugins/bootstrap/css/bootstrap.css')}}">
  <link rel="stylesheet" href="{{ asset('trumbowyg/dist/ui/trumbowyg.css') }}">
</head>
<body>
  @include('admin.template.partials.nav')
      <div class="container">
         <div class="col-md-8 col-md-offset-2">
            <section>
    	         @yield('content')
            </section>
            
        </div>
      </div>
      <script src="{{ asset('js/jquery.min.js') }}"></script>
       <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
       <script src="{{ asset('trumbowyg/dist/trumbowyg.js') }}"></script>
       @yield('js')
         @include('admin.template.partials.footer')
</body>
</html>
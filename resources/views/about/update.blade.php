
@extends ('admin.template.main')
	@section('title','Home')  
	
		@section('content')

         	{!! Form::open(['route' => 'about/update', 'method' => 'PUT']) !!}

         	   <h1>Acerca de mi</h1>

              <div class="form-group">
              	
              	<label class="control-label">A contianuación  podra registrar información personal:</label>
              </div>

              <div class="form-group">
                  {!! Form::hidden('id', $about->id) !!}
              
              		{!! Form::label('description','Descripción') !!}
              		{!! Form::text('description',$about->description,['class'=> 'form-control','placeholder'=>'Escriba el contenido que desea mostrar acerca de usted','required']) !!}
              
              </div>

              
               <div class="form-group">
              
              		{!! Form::label('years','Año de publicación') !!}
              		{!! Form::text('years',$about->years,['class'=> 'form-control','placeholder'=>'Ejemplo:2018','required']) !!}
              
              </div>
          
               <div class="form-group">
              
              		{!! Form::submit('Actualizar',['class'=>'btn btn-primary']) !!}
              		
              
              </div>

         	{!! Form::close() !!}

		@endsection
	
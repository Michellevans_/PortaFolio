@extends('admin.template.main')
@section('content')
	<div>
		<a class="btn btn-success" href="{{ route('about.create') }}">Crear</a>
		<hr>
		<section>
			<table class="table table-hover">
				<thead class="thead-inverse">
					<tr>
						<th>Descripción</th>						
						<th>Años</th>  
					</tr>
				</thead>
				<tbody>
					@foreach($about as $value)
						<tr>
							<td>{{ $value->description }}</td>	
							<td>{{ $value->years }}</td>				
							<td>
								<a class="btn btn-primary btn-xs" href="{{ route('about.edit',['id' => $value->id] )}}" >
									Editar
								</a> 
							</td> 
						</tr>
					@endforeach
				</tbody>
			</table>
	</div>
@endsection
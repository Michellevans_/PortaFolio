
@extends ('admin.template.main')
	@section('title','Home')  
	
		@section('content')

         	{!! Form::open(['route' => 'about.store', 'method' => 'POST']) !!}

         	   <h1>Acerca de mi</h1>

              <div class="form-group">
              	
              	<label class="control-label">A contianuación  podra registrar información personal:</label>
              </div>

              <div class="form-group">
              
              		{!! Form::label('description','Descripción') !!}
              		{!! Form::text('description',null,['class'=> 'form-control','placeholder'=>'Escriba el contenido que desea mostrar acerca de usted','required']) !!}
              
              </div>

              
               <div class="form-group">
              
              		{!! Form::label('years','Año de publicación') !!}
              		{!! Form::text('years',null,['class'=> 'form-control','placeholder'=>'Ejemplo:2018','required']) !!}
              
              </div>
          
               <div class="form-group">
              
              		{!! Form::submit('Registrar',['class'=>'btn btn-primary']) !!}
              		
              
              </div>

         	{!! Form::close() !!}

		@endsection
	
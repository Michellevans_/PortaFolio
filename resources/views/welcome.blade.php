<!DOCTYPE html>

<html lang="en" class="no-js">
   
    <head>
        <meta charset="utf-8"/>
        <title>Michelle Martinez</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="FlameOnePage freebie theme for web startups by FairTech SEO." name="description"/>
        <meta content="FairTech" name="author"/>
        <link href="http://fonts.googleapis.com/css?family=Hind:300,400,500,600,700" rel="stylesheet" type="text/css">
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
        <link href="{{asset('flame/vendor/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('flame/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('flame/css/animate.css')}}" rel="stylesheet">
        <link href="{{asset('flame/vendor/swiper/css/swiper.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('flame/css/layout.min.css')}}" rel="stylesheet" type="text/css"/>
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
   
  
    <body id="body" data-spy="scroll" data-target=".header">

          <header class="header navbar-fixed-top">
              <nav class="navbar" role="navigation">
                <div class="container">
                      <div class="menu-container js_nav-item">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="sr-only">Navegación</span>
                            <span class="toggle-icon"></span>
                        </button>

                          <div class="logo">
                            <a class="logo-wrap" href="#body">
                                <img class="logo-img logo-img-main" src="{{asset('flame/img/logo.png')}}" alt="FlameOnePage Logo">
                                <img class="logo-img logo-img-active" src="{{asset('flame/img/logo-dark.png')}}" alt="FlameOnePage Dark Logo">
                            </a>
                        </div>
                    </div>

                    <div class="collapse navbar-collapse nav-collapse">
                   
                      
                    </div---> 
                    
                        <div class="menu-container">
                            <ul class="nav navbar-nav navbar-nav-right">
                                <li class="js_nav-item nav-item"><a href="/login">¿Eres Admin?</a></li>
                                <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="#body">Incio</a></li>
                                <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="#about">Acerca de mi</a></li>
                                <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="#services">Servicios</a></li>
                                <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="#work">Trabajos</a></li>
                                <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="#contact">Contacto</a></li>
                            </ul>
                        </div>
                   <!-- </div> -->
               </div>
            </nav>
            </header>
       
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <div class="container">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                </ol>
            </div>

            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img class="img-responsive" src="{{asset('flame/img/1920x1080/01.jpg')}}" alt="Slider Image">
                    <div class="container">
                        <div class="carousel-centered">
                            <div class="margin-b-40">
                                <h1 class="carousel-title">Michell Vanessa Martinez Suarez</h1>
                                <p class="color-white">Analista y desarrolladora de software</p>
                            </div>
                            <a href="#" class="btn-theme btn-theme-sm btn-white-brd text-uppercase">Detalles</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img class="img-responsive" src="{{asset('flame/img/1920x1080/02.jpg')}}" alt="Slider Image">
                    <div class="container">
                        <div class="carousel-centered">
                            <div class="margin-b-40">
                                <h2 class="carousel-title">Michell Vanessa Martinez Suarez</h2>
                                <p class="color-white">Analista y desarrolladora de software</p>
                            </div>
                            <a href="#" class="btn-theme btn-theme-sm btn-white-brd text-uppercase">Detalle</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="about">
              <h2>Acerca de mi</h2>
              @foreach($about as $value)
            <div class="content-lg container">
                <!-- Masonry Grid -->
                <div class="masonry-grid row">
                    <div class="masonry-grid-sizer col-xs-6 col-sm-6 col-md-1"></div>
                    <div class="masonry-grid-item col-xs-12 col-sm-6 col-md-4 sm-margin-b-30">
                        <div class="margin-b-60">
                          
                            
                            <p>{{$value->description}} </p>
                            <p>{{$value->years}} </p>
                        </div>
                        <img class="full-width img-responsive wow fadeInUp" src="{{asset('flame/img/500x500/01.jpg')}}" alt="Portfolio Image" data-wow-duration=".3" data-wow-delay=".2s">
                    </div>
                  
                   
                </div>
                @enforeach
                <!-- End Masonry Grid -->
            </div>
            
            
                            <!-- End Accodrion -->
                        </div>
                    <!-- </div> -->
                    <!--// end row -->
               <!-- </div> -->
            <!-- </div> -->
       <!-- </div> -->
       

        <!-- Work -->
        <div id="work">
            <div class="section-seperator">
                 <h2>Trabajos</h2>
                  @foreach($job as $value)
                <div class="content-md container">
                    <div class="row margin-b-40">
                        <div class="col-sm-6">
                           
                           

                            <p>{{$value->job_title}}</p>
                            <p>{{$value->deadline}}</p>
                            <p>{{$value->job_description}}</p>
                        </div>
                    </div>

                 @endforeach                    
                 <!--// end row -->

                    <!-- Masonry Grid -->
                    <div class="masonry-grid row">


                       
                    
                    
                            <!-- End Work -->
                        </div>
                    </div>
                    <!-- End Masonry Grid -->
                </div>
            </div>
            
          
                <!-- End Swiper Clients -->
           <!-- </div> -->
            <!-- End Clients -->
       <!-- </div> -->
        <!-- End Work -->

        <!-- Services -->
        <div id="services">
            <div class="bg-color-sky-light" data-auto-height="true">
                <div class="content-lg container">
                    <div class="row margin-b-40">
                        <div class="col-sm-6">
                            <h2>Servicios</h2>
                            <p>Aqui encontrara los servicios prestados</p>
                        </div>
                    </div>
                    <!--// end row -->
                    foreach($servicie as $value)

                    <div class="row row-space-1 margin-b-2">
                        <div class="col-sm-4 sm-margin-b-2">
                            <div class="service" data-height="height">
                                <div class="service-element">
                                    <i class="service-icon icon-chemistry"></i>
                                </div>
                                <div class="service-info">
                                    <h3>{{$value->name_servicies}}</h3>
                                    <p class="margin-b-5">{{$value->description}}</p>
                                    <p class="margin-b-5">{{$value->date}}</p>
                                </div>
                                <a href="#" class="content-wrapper-link"></a>    
                            </div>
                        </div>
                     
                       
                    <!--// end row -->
                       
                    <!--// end row -->

            
                    <!--// end row -->
                </div>
                @endforeach
            </div>
        </div>
    </div>
        <!-- End Service -->
            
        <!-- Contact -->
        <div id="contact">
            <!-- Contact List -->
            <div class="section-seperator">
                <div class="content-lg container">
                    <div class="row">
                        <!-- Contact List -->
                        <div class="col-sm-4 sm-margin-b-50">
                            <h3><a href="http://ft-seo.ch/">Contacto personal</a> <span class="text-uppercase margin-l-20">Human Resources</span></h3>
                            <p>Aqui encontraras los telefonos y correos para estar en contacto</p>
                            <ul class="list-unstyled contact-list">
                                <li><i class="margin-r-10 color-base icon-call-out"></i> 3193249827</li>
                                <li><i class="margin-r-10 color-base icon-envelope"></i> miche.vanessa2@gmail.com</li>
                            </ul>
                        </div>
                        <!-- End Contact List -->

                        <!-- Contact List -->
                     
                        <!-- End Contact List -->

                        <!-- Contact List -->
                       
                        <!-- End Contact List -->
                    </div>
                    <!--// end row -->
                </div>
            </div>
            <!-- End Contact List -->
            
    </div>
        <!-- End Contact -->
        <!--========== END PAGE LAYOUT ==========-->

        <!--========== FOOTER ==========-->
        <footer class="footer">
            <!-- Links -->
           
            <!-- End Links -->

            <!-- Copyright -->
            
                <!--// end row -->
           
            <!-- End Copyright -->
        </footer>
        <!--========== END FOOTER ==========-->

        <!-- Back To Top -->
        <a href="javascript:void(0);" class="js-back-to-top back-to-top">Top</a>

        <!-- JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- CORE PLUGINS -->
        <script src="{{asset('flame/vendor/jquery.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('flame/vendor/jquery-migrate.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('flame/vendor/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>

        <!-- PAGE LEVEL PLUGINS -->
        <script src="{{asset('flame/vendor/jquery.easing.js')}}" type="text/javascript"></script>
        <script src="{{asset('flame/vendor/jquery.back-to-top.js')}}" type="text/javascript"></script>
        <script src="{{asset('flame/vendor/jquery.smooth-scroll.js')}}" type="text/javascript"></script>
        <script src="{{asset('flame/vendor/jquery.wow.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('flame/vendor/swiper/js/swiper.jquery.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('flame/vendor/masonry/jquery.masonry.pkgd.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('flame/vendor/masonry/imagesloaded.pkgd.min.js')}}" type="text/javascript"></script>

        <!-- PAGE LEVEL SCRIPTS -->
        <script src="{{asset('flame/js/layout.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('flame/js/components/wow.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('flame/js/components/swiper.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('flame/js/components/masonry.min.js')}}" type="text/javascript"></script>

    </body>
    <!-- END BODY -->
</html>
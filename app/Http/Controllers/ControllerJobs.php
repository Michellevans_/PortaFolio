<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Job as jobs;

class ControllerJobs extends Controller
{
    public function index()
	{
		$job = jobs::all();
		return \View::make('job/list', compact('job'));
	}

	public function create()
	{
         return \View::make('job/create');
	}

	public function store(Request $request)
	{
 		$job = new jobs;
 		$job->job_title = $request->job_title;
 		$job->deadline = $request->deadline;
 		$job->job_description = $request->job_description;
	   	$job->save();
	   	return \View::make('job/list');
	}

	public function show($id)
	{

	}

	public function edit($id)
	{
		$job = jobs::find($id);
		return \View::make('job/update', compact('job'));
	}

	public function update(Request $request)
	{
		$job = jobs::find($request->id);
 		$job->job_title = $request->job_title;
 		$job->deadline = $request->deadline;
 		$job->job_description = $request->job_description;
	   	$job->save();
	   	return \View::make('job/list');
	}

	public function destroy($id)
	{
		
	}
}

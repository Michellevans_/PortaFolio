<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AboutMe as AboutMe;

class ControllerAboutMe extends Controller
{
	public function index()
	{
		$about = AboutMe::all();
		return \View::make('about/list', compact('about'));
	}

	public function create()
	{
		return \View::make('about/create');
	}

	public function store(Request $request)
	{
		$about = new AboutMe;
		$about->description = $request->description;
		$about->years = $request->years;
		$about->person_id = '1';
		$about->save();
		return \View::make('about/list'); 
	}

	public function edit($id)
	{
		$about = AboutMe::find($id);
		return \View::make('about/update', compact('about'));
	}

	public function update(Request $request)
	{
		$about = AboutMe::find($request->id);
		$about->description = $request->description;
		$about->years = $request->years;
		$about->person_id = '1';
		$about->save();
		return \View::make('about/list');
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Servicie;

class ControllerServicies extends Controller
{
    public function index()
	{
		$service = Servicie::all();
		return \View::make('service/list', compact('service'));
	}

	public function create()
	{
		return \View::make('service/create');
	}

	public function store(Request $request)
	{
		$service = new Servicie;
		$service->name_servicies = $request->name_services;
		$service->description = $request->description;
		$service->date = $request->date;
		$service->person_id = '1';
		$service->save();
		return \View::make('service/list');
	}

	public function show($id)
	{

	}

	public function edit($id)
	{
		$service = Servicie::find($id);
		return \View::make('service/update', compact('service'));
	}

	public function update(Request $request)
	{
		$service = Servicie::find($request->id);
		$service->name_servicies = $request->name_services;
		$service->description = $request->description;
		$service->date = $request->date;
		$service->person_id = '1';
		$service->save();
		return \View::make('service/list');
	}

	public function destroy($id)
	{
		
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AboutMe;
use App\Models\Job;
use App\Models\Servicie;

class WelcomeController extends Controller
{
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
       $about = AboutMe::all();
       $job = Job::all();
       $servicie = Servicie::all();
       return \View::make('welcome',compact('about','job','servicie'));
        
    }

}

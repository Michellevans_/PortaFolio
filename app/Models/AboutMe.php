<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutMe extends Model
{
     protected $table="about_me";
    protected $fillable=['description','years', 'person_id'];
    protected $guarded = ['id'];

   public function People()
   {
   	 return $this->hasMany('App\Models\Person');
   }

}

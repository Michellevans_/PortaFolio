<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servicie extends Model
{
    protected $table="servicies";
    protected $fillable=['name_servicies','description','date', 'person_id'];
    protected $guarded = ['id'];

    public function people()
    {
    	$this->hasMany(Person::class);
    }
}

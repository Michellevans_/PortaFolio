<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table="people";
    protected $fillable=[ 'name','lastname','email','cel','Address', 'city_id'];
    protected $guarded = ['id'];

   public function aboutMe()
   {

   	 return $this->hasMany('App\Models\AboutMe');
   }

    public function user()
   {

   	 return $this->belongsTo('App\User');
   }

   public function city()
   {
     return $this->belongsTo('App\Models\City');
   }

   public function detailJobPeople()
   {
    return $this->hasMany(detailJobPeople::class);
   }

   public function services()
   {
    return $this->belongsTo(Service::class);
   }

}

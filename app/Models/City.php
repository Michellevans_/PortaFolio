<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table="cities";
    protected $fillable=['departmentName'];
    protected $guarded = ['id'];

 public function people()
 {
 	$this->hasMany('App\Models\Person');
 }

 


}

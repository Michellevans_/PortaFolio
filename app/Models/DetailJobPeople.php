<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailJobPeople extends Model
{
    protected $table = "detail_job_people";
    protected $fillable = ['job_id', 'person_id'];
    protected $guarded = ['id'];

    public function people()
    {
    	return $this->belongsTo(Person::class);
    }

    public function jobs()
    {
    	return $this->belongsTo(Jobs::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table="jobs";
    protected $fillable=[ 'job_title','deadline','job_description'];
    protected $guarded = ['id'];

    public function detailJobPeople()
    {
    	$this->hasMany(DetailJobPeople::class);
    }
}

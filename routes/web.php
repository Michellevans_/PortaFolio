<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/*Route::group(['prefix'=>'admin'],function(){

	Route::resource('about','ControllerAboutMe');
 
});*/
Route::resource('about', 'ControllerAboutMe');

Route::get('about/destroy/{id}', ['as' => 'about/destroy','uses'=>'ControllerAboutMe@destroy']);
//Route for search method
Route::post('about/search',['as'=>'about/search','uses'=>'ControllerAboutMe@search']);
//Route for update method
Route::put('about/update',['as' => 'about/update','uses'=>'ControllerAboutMe@update']);
//Route for store method
Route::post('about/store',['as' => 'about/store','uses'=>'ControllerAboutMe@store']);
//Route for searchInstructors methodcity

Route::resource('city','ControllerCities');

Route::get('city/destroy/{id}', ['as' => 'city/destroy','uses'=>'ControllerCities@destroy']);
//Route for search method
Route::post('city/search',['as'=>'city/search','uses'=>'ControllerCities@search']);
//Route for update method
Route::put('city/update',['as' => 'city/update','uses'=>'ControllerCities@update']);
//Route for store method
Route::post('city/store',['as' => 'city/store','uses'=>'ControllerCities@store']);
//Route for searchInstructors method

Route::resource('jobs','ControllerJobs');

Route::get('jobs/destroy/{id}', ['as' => 'jobs/destroy','uses'=>'ControllerJobs@destroy']);
//Route for search method
Route::post('jobs/search',['as'=>'jobs/search','uses'=>'ControllerJobs@search']);
//Route for update method
Route::put('jobs/update',['as' => 'jobs/update','uses'=>'ControllerJobs@update']);
//Route for store method
Route::post('jobs/store',['as' => 'jobs/store','uses'=>'ControllerJobs@store']);
//Route for searchInstructors method 

Route::resource('services','ControllerServicies');

Route::get('services/destroy/{id}', ['as' => 'services/destroy','uses'=>'ControllerServicies@destroy']);
//Route for search method
Route::post('services/search',['as'=>'services/search','uses'=>'ControllerServicies@search']);
//Route for update method
Route::put('services/update',['as' => 'services/update','uses'=>'ControllerServicies@update']);
//Route for store method
Route::post('services/store',['as' => 'services/store','uses'=>'ControllerServicies@store']);
//Route for searchInstructors method
 
Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/', 'WelcomeController@index');
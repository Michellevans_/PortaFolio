<?php

use Illuminate\Database\Seeder;
use App\Models\Person;

class PeopleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $people = array(
        	[
        		'document' => '123456789',
        		'name' => 'Michelle',
        		'lastname' => 'Martinez',
        		'email' => 'mvmartinez@misena.edu.co',
        		'cel' => '3216874597',
        		'address' => 'Calle 39 d Sur # 2R-21',
        		'city_id' => '1',
        	]
        );

        foreach ($people as $value) {
        	$people = new Person;
        	$people->document = $value['document'];
        	$people->name = $value['name'];
        	$people->lastname = $value['lastname'];
        	$people->email = $value['email'];
        	$people->cel = $value['cel'];
        	$people->address = $value['address'];
        	$people->city_id = $value['city_id'];
        	$people->save();
        }
    }
}

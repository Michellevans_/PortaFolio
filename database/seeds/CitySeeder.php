<?php

use Illuminate\Database\Seeder;
use App\Models\City;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $city = array(
        	[
        		'department_name' => 'Bogota D.C',
        	]
        );

        foreach ($city as $value) {
        	$city = new City;
        	$city->department_name = $value['department_name'];
        	$city->save();
        }
    }
}

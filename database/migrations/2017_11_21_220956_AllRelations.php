<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('about_me', function($table){
            $table->foreign('person_id')
            ->references('id')
            ->on('people');
        });
        Schema::table('detail_job_peoples', function($table){
            $table->foreign('person_id')
            ->references('id')
            ->on('people')
            ->onUpdate('cascade');

            $table->foreign('job_id')
            ->references('id')
            ->on('jobs')
            ->onUpdate('cascade');
        });
        Schema::table('people', function($table){
            $table->foreign('city_id')
            ->references('id')
            ->on('cities')
            ->onUpdate('cascade');
        });
        Schema::table('servicies', function($table){
            $table->foreign('person_id')
            ->references('id')
            ->on('people')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
